#include <math.h>
#include "potentials.h"

double fene_potential(const double r2)
{
    const double R2 = 2.25;
    return -15. * R2 * log(1 - r2 / R2);
}

double fene_potential_del_1_div_r(const double r2)
{
    const double R2inv = 1 / 2.25;
    return 30. / (1 - r2 * R2inv);
}

double fene_potential_del_2(const double r2)
{
    const double R2 = 2.25;
    return 30. * R2 * (R2 + r2) / ((R2 - r2) * (R2 - r2));
}

double lj_potential(const double r2, const double epsilon, const double sigma)
{
    double r6inv = sigma * sigma / r2;
    r6inv *= r6inv * r6inv;
    return 4 * epsilon * (r6inv * r6inv - r6inv + 1. / 4);
}

double lj_potential_del_1_div_r(const double r2, const double epsilon, const double sigma)
{
	double r6inv = sigma * sigma / r2;
    r6inv *= r6inv * r6inv;

	return -4 * epsilon * (12 * r6inv * r6inv - 6 * r6inv) / r2;
}

double lj_potential_del_2(const double r2, const double epsilon, const double sigma)
{
    double r6inv = sigma * sigma / r2;
    r6inv *= r6inv * r6inv;
    return 4 * epsilon * r6inv * (12 * 13 * r6inv - 6 * 7) / r2;
}
