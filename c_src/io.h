#include <signal.h>
#include <time.h>
#include <hdf5.h>

extern volatile int NoKeyboardInterrupt;
void intHandler(int dummy);

int initialize(MySystem * s, int argc, char *argv[]);
void finalize(MySystem * s);
void read_conf_scalars(MySystem * s);
void read_conf_arrays(MySystem * s);
void read_dataset(hid_t file_id, void *data, char *name, hid_t dtype_id);
void write_dataset(hid_t file_id, void *data, char *name, int rank, hsize_t * current_dims, hid_t dtype_id);
void save_conf(MySystem * s);
void save_ana(MySystem * s);
void write_simulation_data(MySystem * s);
int print_info(MySystem * s, struct timespec time_ref);
