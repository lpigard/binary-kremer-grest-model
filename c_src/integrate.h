void integrate(MySystem * s);
void vv_step_one(MySystem * s);
void vv_step_two(MySystem * s);
void fill_cl(MySystem * s);
void fill_nl(MySystem * s);
void forces(MySystem * s);
