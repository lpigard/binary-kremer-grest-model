void init_mesh(MySystem *s);
#pragma acc routine seq
double wrap_back_dis(double d, double l);
#pragma acc routine seq
int id_cell(MySystem *s, int i);
void calc_cell_neighbor(MySystem *s);
